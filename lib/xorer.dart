import 'dart:io';

Future<void> xor({
  required File input1,
  required File input2,
  required File output,
  XORStrategy strategy = XORStrategy.loopSmallerFile,
}) async {
  final size1 = await input1.length();
  final size2 = await input2.length();

  final bytes = await Future.wait([
    input1.readAsBytes(),
    input2.readAsBytes(),
  ]);

  if (size1 == size2) {
    await output.writeAsBytes(List<int>.generate(
      size1,
      (i) => bytes[0][i] ^ bytes[1][i],
    ));
  } else {
    if (strategy == XORStrategy.loopSmallerFile) {
      if (size1 > size2) {
        await output.writeAsBytes(List<int>.generate(
          size1,
          (i) => bytes[0][i] ^ bytes[1][i % size2],
        ));
      } else {
        await output.writeAsBytes(List<int>.generate(
          size2,
          (i) => bytes[0][i % size1] ^ bytes[1][i],
        ));
      }
    } else if (strategy == XORStrategy.pasteTailOfBiggerFile) {
      if (size1 > size2) {
        await output.writeAsBytes(List<int>.generate(
          size1,
          (i) {
            if (i > size2 - 1) {
              return bytes[0][i];
            } else {
              return bytes[0][i] ^ bytes[1][i];
            }
          },
        ));
      } else {
        await output.writeAsBytes(List<int>.generate(
          size2,
          (i) {
            if (i > size1 - 1) {
              return bytes[1][i];
            } else {
              return bytes[0][i] ^ bytes[1][i];
            }
          },
        ));
      }
    } else {
      throw UnimplementedError();
    }
  }
}

enum XORStrategy { loopSmallerFile, pasteTailOfBiggerFile }
