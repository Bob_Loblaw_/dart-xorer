import 'dart:io';
/*import 'dart:math';*/

import 'package:xorer/xorer.dart' as xorer;

/// This was tested in an XOR-triangle test and it did great!
void main(List<String> arguments) async {
  final curDir = Directory.current.absolute.path;
  final pic1 = File('$curDir/assets/');
  final pic2 = File('/home/hawken/Desktop/maze_xor_rnd');
  await xorer.xor(
    input1: pic1,
    input2: pic2,
    output: File('/home/hawken/Desktop/tmp_test'),
  );
  print('DONE!');
  /* final curDir = Directory.current.absolute.path;
  final file = File('$curDir/assets/secret');
  final rnd = File('$curDir/assets/rev');
  final mazeSize = await file.length();
  final fileBytes = await file.readAsBytes();
  await rnd.writeAsBytes(List<int>.generate(mazeSize, (index) {
    return fileBytes[index] ^ 0xff;
  }));
  print('DONE!'); */
}
